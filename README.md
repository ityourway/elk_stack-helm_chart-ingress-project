# ELK-filebeat project with helm chart and ingress

## Links

Artifact Hub: https://artifacthub.io/
Helm Docs: https://helm.sh/docs/
Ingress Nginx: https://kubernetes.github.io/ingress-...

## Helm chart

helm install elasticsearch .
helm install logstash .
helm install filebeat .
helm install kibana .

